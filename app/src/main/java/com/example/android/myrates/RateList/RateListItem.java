package com.example.android.myrates.RateList;

public class RateListItem {

    private String mCurrency;
    private String mCurrencyValue;

    public RateListItem(String currency, String currencyValue) {
        mCurrency = currency;
        mCurrencyValue = currencyValue;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public String getCurrencyValue() {
        return mCurrencyValue;
    }

}
