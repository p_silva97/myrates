package com.example.android.myrates.CurrencyConversion;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.android.myrates.R;

import java.util.Objects;

public class CurrencyConversionFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private EditText mEditTextBaseCurrency;
    private EditText mEditTextSelectedCurrency;

    private String rateValue;

    private Boolean calcOpen;

    public CurrencyConversionFragment(String currency, String rate) {
        Bundle arg = new Bundle();
        arg.putString("Currency", currency);
        arg.putString("currencyValue", rate);
        setArguments(arg);
    }

    private float calculateValueFromBase(String baseCurrencyValue, String selectedRateValue) {
        if (TextUtils.isEmpty(baseCurrencyValue) || TextUtils.isEmpty(selectedRateValue)){
            return 0f;
        }

        float integBase = Float.parseFloat(baseCurrencyValue);
        float integRate = Float.parseFloat(selectedRateValue);
        return integBase * integRate;
    }

    private float calculateValueFromSelected(String selectedCurrencyValue, String selectedRateValue) {
        if (TextUtils.isEmpty(selectedCurrencyValue) || TextUtils.isEmpty(selectedRateValue)){
            return 0f;
        }

        float integBase = Float.parseFloat(selectedCurrencyValue);
        float integRate = Float.parseFloat(selectedRateValue);
        return integBase / integRate;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_currency_conversion, container, false);

        rateValue = getArguments() != null ? getArguments().getString("currencyValue") : null;
        String currencyValue = getArguments().getString("Currency");
        calcOpen = false;

        Spinner mSpinnerBaseValue = view.findViewById(R.id.currency_base_value);
        Spinner mSpinnerSelectedValue = view.findViewById(R.id.currency_selected_value);

        ArrayAdapter<CharSequence> adapterBase = ArrayAdapter.createFromResource(Objects.requireNonNull(getContext()), R.array.pref_currency_options, android.R.layout.simple_spinner_item);
        adapterBase.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerBaseValue.setAdapter(adapterBase);
        mSpinnerBaseValue.setSelection(adapterBase.getPosition(PreferenceManager.getDefaultSharedPreferences(getContext()).getString("currency", "EUR")));

        ArrayAdapter<CharSequence> adapterSel = ArrayAdapter.createFromResource(getContext(), R.array.pref_currency_options, android.R.layout.simple_spinner_item);
        adapterSel.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerSelectedValue.setAdapter(adapterSel);
        mSpinnerSelectedValue.setSelection(adapterSel.getPosition(currencyValue));

        mEditTextBaseCurrency = view.findViewById(R.id.base_value);
        mEditTextSelectedCurrency = view.findViewById(R.id.selected_value);

        mEditTextBaseCurrency.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!mEditTextBaseCurrency.hasFocus() && calcOpen) {
                    return;
                }
                float floatVal = calculateValueFromBase(mEditTextBaseCurrency.getText().toString(), rateValue);
                mEditTextSelectedCurrency.setText(String.valueOf(floatVal));
                calcOpen = true;
            }
        });

        mEditTextSelectedCurrency.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!mEditTextSelectedCurrency.hasFocus()) {
                    return;
                }
                float floatVal = calculateValueFromSelected(mEditTextSelectedCurrency.getText().toString(), rateValue);
                mEditTextBaseCurrency.setText(String.valueOf(floatVal));
            }
        });

        mEditTextBaseCurrency.setText("100");

        return view;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
