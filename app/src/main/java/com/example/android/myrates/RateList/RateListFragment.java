package com.example.android.myrates.RateList;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.android.myrates.NetworkUtils;
import com.example.android.myrates.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

public class RateListFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mRateListAdapter;

    private RequestQueue mRequestQueue;
    private ArrayList<RateListItem> mRateList;
    private ProgressBar mLoadingIndicator;

    private TextView mCurrentCurrency;
    private TextView mDateRetrieved;

    @Override
    public void onResume() {
        super.onResume();
        String retrievedCurrency = PreferenceManager.getDefaultSharedPreferences(Objects.requireNonNull(getContext())).getString("currency", "EUR");
        retrieveRatesInformation(retrievedCurrency);

        mRateList = new ArrayList<>();
        mRequestQueue = Volley.newRequestQueue(getContext());

        mCurrentCurrency.setText(getResources().getString(R.string.current_currency, retrievedCurrency));
        mDateRetrieved.setText(getResources().getString(R.string.information_retrieved_on, NetworkUtils.CURRENT_DATE));
    }

    private void retrieveRatesInformation(String currency) {
        URL madeUrl = NetworkUtils.latestRatesUrl(currency);
        new QueryTask().execute(madeUrl);
        Log.i("URL: ", madeUrl.toString());
    }

    @SuppressLint("StaticFieldLeak")
    public class QueryTask extends AsyncTask<URL, Void, JsonObjectRequest> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mLoadingIndicator.setVisibility(View.VISIBLE);
        }

        @Override
        protected JsonObjectRequest doInBackground(URL... params) {
            URL searchUrl = params[0];

            return new JsonObjectRequest(Request.Method.GET, searchUrl.toString(), null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {

                                JSONObject jsonObject = response.getJSONObject("rates");
                                Iterator<String> keys = jsonObject.keys();

                                while (keys.hasNext()) {
                                    String currency = keys.next();
                                    String currencyVal = jsonObject.getString(currency);

                                    mRateList.add(new RateListItem(currency, currencyVal));
                                }

                                mRateListAdapter = new RateListAdapter(getContext(), mRateList);
                                mRecyclerView.setAdapter(mRateListAdapter);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });

        }

        @Override
        protected void onPostExecute(JsonObjectRequest request) {
            mRequestQueue.add(request);
            mLoadingIndicator.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rate_list, container, false);

        mLoadingIndicator = view.findViewById(R.id.pb_loading_indicator);
        mCurrentCurrency = view.findViewById(R.id.current_currency);
        mDateRetrieved = view.findViewById(R.id.date_retrieved);

        mRecyclerView = view.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return view;
    }

}
