package com.example.android.myrates.FloatRate;

import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.android.myrates.NetworkUtils;
import com.example.android.myrates.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

public class FloatRateFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mFloatRateAdapter;

    private RequestQueue mRequestQueue;
    private ArrayList<FloatRateItem> mFloatRate;
    private ProgressBar mLoadingIndicator;

    private TextView mSelectedCurrency;
    private TextView mDateRetrieved;

    private static String SELECTED_CURRENCY = null;

    @Override
    public void onResume() {
        super.onResume();
        String CURRENT_CURRENCY = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("currency", "EUR");

        retrieveRatesInformation(CURRENT_CURRENCY);

        //starts a new AsyncTask that retrieves the parsed JSON
        mFloatRate = new ArrayList<>();
        mRequestQueue = Volley.newRequestQueue(getContext());

        //adds the information retrieved to the TextViews
        mSelectedCurrency.setText(getResources().getString(R.string.selected_currency, SELECTED_CURRENCY));
        mDateRetrieved.setText(getResources().getString(R.string.information_retrieved_on_float_rate, NetworkUtils.CURRENT_DATE));
    }

    public void getSelectedCurrency(String selectedCurrency) {
        SELECTED_CURRENCY = selectedCurrency;
    }

    private void retrieveRatesInformation(String currency) {
        URL madeUrl = NetworkUtils.floatRatesUrl(SELECTED_CURRENCY, currency);
        new QueryTask().execute(madeUrl);
        Log.i("URL: ", madeUrl.toString());
    }

    public class QueryTask extends AsyncTask<URL, Void, JsonObjectRequest> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mLoadingIndicator.setVisibility(View.VISIBLE);
        }

        @Override
        protected JsonObjectRequest doInBackground(URL... params) {
            URL searchUrl = params[0];

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, searchUrl.toString(), null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {

                                JSONObject jsonObject = response.getJSONObject("rates");
                                Iterator<String> keys = jsonObject.keys();

                                while (keys.hasNext()) {
                                    String date = (String) keys.next();
                                    JSONObject currencyObj = jsonObject.getJSONObject(date);
                                    String currencyVal = currencyObj.getString(SELECTED_CURRENCY);
                                    mFloatRate.add(new FloatRateItem(date, currencyVal));
                                }

                                mFloatRateAdapter = new FloatRateAdapter(getContext(), mFloatRate);
                                mRecyclerView.setAdapter(mFloatRateAdapter);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });

            return request;

        }

        @Override
        protected void onPostExecute(JsonObjectRequest request) {
            mRequestQueue.add(request);
            mLoadingIndicator.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_float_rate, container, false);

        //finds the views
        mLoadingIndicator = view.findViewById(R.id.pb_loading_indicator_float_rate);
        mSelectedCurrency = view.findViewById(R.id.selected_currency);
        mDateRetrieved = view.findViewById(R.id.date_retrieved_float_rate);

        //
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return view;
    }

}
