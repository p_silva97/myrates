package com.example.android.myrates.FloatRate;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.myrates.R;
import com.example.android.myrates.RateList.RateListAdapter;

import java.util.ArrayList;

public class FloatRateAdapter extends RecyclerView.Adapter<FloatRateAdapter.FloatRateViewHolder>{

    private Context mContext;
    private ArrayList<FloatRateItem> mFloatRateList;

    public FloatRateAdapter(Context context, ArrayList<FloatRateItem> floatRateList) {
        mContext = context;
        mFloatRateList = floatRateList;
    }

    @NonNull
    @Override
    public FloatRateViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.float_rate_item, parent, false);
        return new FloatRateViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FloatRateViewHolder holder, int position) {
        FloatRateItem currentItem = mFloatRateList.get(position);

        String dateRetrieved = currentItem.getDateRetrieved();
        String currencyVal = currentItem.getCurrencyValue();

        holder.mTextViewDateRetrieved.setText(dateRetrieved);
        holder.mTextViewCurrencyValue.setText(currencyVal);
    }

    @Override
    public int getItemCount() {
        return mFloatRateList.size();
    }

    public class FloatRateViewHolder extends RecyclerView.ViewHolder {

        public TextView mTextViewDateRetrieved;
        public TextView mTextViewCurrencyValue;

        public FloatRateViewHolder(@NonNull final View itemView) {
            super(itemView);

            mTextViewDateRetrieved = itemView.findViewById(R.id.date_rate_retrieved);
            mTextViewCurrencyValue = itemView.findViewById(R.id.currency_value_at_date);
        }

    }

}
