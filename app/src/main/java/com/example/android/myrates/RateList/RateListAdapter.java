package com.example.android.myrates.RateList;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.myrates.CurrencyConversion.CurrencyConversionFragment;
import com.example.android.myrates.FloatRate.FloatRateFragment;
import com.example.android.myrates.R;

import java.util.ArrayList;

public class RateListAdapter extends RecyclerView.Adapter<RateListAdapter.RateListViewHolder> {

    public String SELECTED_VALUE = null;

    private Context mContext;
    private ArrayList<RateListItem> mRateList;

    public RateListAdapter(Context context, ArrayList<RateListItem> rateList) {
        mContext = context;
        mRateList = rateList;
    }

    @NonNull
    @Override
    public RateListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.rate_list_item, parent, false);
        return new RateListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RateListViewHolder holder, int position) {
        RateListItem currentItem = mRateList.get(position);

        String currencyName = currentItem.getCurrency();
        String currencyVal = currentItem.getCurrencyValue();

        holder.mTextViewCurrency.setText(currencyName);
        holder.mTextViewCurrencyValue.setText(currencyVal);
    }

    @Override
    public int getItemCount() {
        return mRateList.size();
    }

    public class RateListViewHolder extends RecyclerView.ViewHolder {

        public TextView mTextViewCurrency;
        public TextView mTextViewCurrencyValue;

        public RateListViewHolder(@NonNull final View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(itemView.getContext(), mRateList.get(getAdapterPosition()).getCurrency(), Toast.LENGTH_SHORT).show();
                    new FloatRateFragment().getSelectedCurrency(mRateList.get(getAdapterPosition()).getCurrency());
                    ((AppCompatActivity)mContext).getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new CurrencyConversionFragment(mRateList.get(getAdapterPosition()).getCurrency(), mRateList.get(getAdapterPosition()).getCurrencyValue())).commit();
                }
            });

            mTextViewCurrency = itemView.findViewById(R.id.currency);
            mTextViewCurrencyValue = itemView.findViewById(R.id.currency_value);
        }

    }

}
