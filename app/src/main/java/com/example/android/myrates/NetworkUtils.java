package com.example.android.myrates;

import android.net.Uri;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class NetworkUtils {

    private final static String EXCHANGE_RATES_API_BASE_URL = "https://api.exchangeratesapi.io/";
    private final static String PARAM_START_DATE = "start_at";
    private final static String PARAM_END_DATE = "end_at";
    private final static String PARAM_CURRENCY = "symbols";
    private final static String PARAM_HISTORY = "history";
    private final static String PARAM_BASE_CURRENCY = "base";

    public static String CURRENT_DATE = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    private static String COMPARE_DATE = LocalDate.now().minusMonths(1).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

    public static URL latestRatesUrl(String requestedCurrency) {

        Uri uri = Uri.parse(EXCHANGE_RATES_API_BASE_URL).buildUpon()
                .appendPath(CURRENT_DATE)
                .appendQueryParameter(PARAM_BASE_CURRENCY, requestedCurrency)
                .build();

        URL url = null;

        try {
            url = new URL(uri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return url;

    }

    public static URL floatRatesUrl(String selectedCurrency, String requestedCurrency) {

        Uri uri = Uri.parse(EXCHANGE_RATES_API_BASE_URL).buildUpon()
                .appendPath(PARAM_HISTORY)
                .appendQueryParameter(PARAM_START_DATE, COMPARE_DATE)
                .appendQueryParameter(PARAM_END_DATE, CURRENT_DATE)
                .appendQueryParameter(PARAM_CURRENCY, selectedCurrency)
                .appendQueryParameter(PARAM_BASE_CURRENCY, requestedCurrency)
                .build();

        URL url = null;

        try {
            url = new URL(uri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return url;

    }

}
