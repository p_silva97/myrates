package com.example.android.myrates.FloatRate;

public class FloatRateItem {

    private String mDateRetrieved;
    private String mCurrencyValue;

    public FloatRateItem(String dateRetrieved, String currencyValue) {
        mDateRetrieved = dateRetrieved;
        mCurrencyValue = currencyValue;
    }

    public String getDateRetrieved() {
        return mDateRetrieved;
    }

    public String getCurrencyValue() {
        return mCurrencyValue;
    }

}
